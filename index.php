<?php
$pageTitle = 'Página Principal';
?>

<!DOCTYPE html>
<html lang="en">
  <?php include("./src/components/header.php") ?>

  <body>
    <?php
      if(empty($_COOKIE['isAuthenticated'])) {
        include("./src/components/noAuthenticatedIndex.php");
      } else if($_COOKIE['userProfile'] === 'Usuário') {
        include("./src/components/authenticatedIndexUser.php");
      } else if($_COOKIE['userProfile'] === 'Prestador') {
        include("./src/components/authenticatedIndexProvider.php");
      } else {
        include("./src/actions/logout.php");
      }
    ?>
  </body>
</html>