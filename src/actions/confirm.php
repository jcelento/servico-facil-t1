<?php
function redirect_to_home() {
  header('Location: ' . '../../index.php');
  exit();
}

$userProfile = null;

function check_user_validity($token) {
  /*
    Deve buscar o usuário pela token no banco, se existir,
    trocar o estado dele para ativo

    Deve também trazer o perfil do usuário
    se o usuário for válido, retorne true, se não retorne false
  */

  $userProfile = 'FALSEY_USER_PROFILE';
}

$userToken = $_GET['userToken'];

if(empty($userToken) || !check_user_validity($userToken)) {
  redirect_to_home();
}

setcookie('isAuthenticated', true, 0, '/');
setcookie('userProfile', $userProfile, 0, '/');
setcookie('userToken', $userToken, 0, '/');

// Redirect para a página inicial
redirect_to_home();
?>