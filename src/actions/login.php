<?php
function redirect_to_login() {
  header('Location: ' . '../../login.php?error=true');
  exit();
}

function test_user_existance($email, $password) {
  return true;
}

$userEmail = $_POST['userEmail'];
$userPassword = $_POST['userPassword'];

if(empty($userEmail) || empty($userPassword)) {
  redirect_to_login();
}
/*
  A FAZER:
  - Testar se o usuário existe no Banco
  - Testar se as informações do usuários estão corretas
  - Pegar a userToken
  - Pegar o perfil do usuário

  João: Acho que é possível fazer os dois num query só.
*/

$userIsValid = test_user_existance(
  $userEmail,
  md5($userPassword)
);

if(!$userIsValid) {
  return redirect_to_login();
}

$userToken = 'FALSEY_USER_TOKEN'; // Mudar
$userProfile = 'Usuário'; // Mudar

setcookie('isAuthenticated', true, 0, '/');
setcookie('userProfile', $userProfile, 0, '/');
setcookie('userToken', $userToken, 0, '/');

// Redirect para a página inicial
header('Location: ' . '../../index.php');
?>