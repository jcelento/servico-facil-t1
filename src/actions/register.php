<?php
$siteURL = $_SERVER['SERVER_NAME'] . '/servico-facil-t1';

function redirect_to_register() {
  header("Location:../../register.php?" . "error=true");
  exit();
}

function generate_token($email) {
  return md5( $email . microtime(false) );
}

function test_user_validity($email, $password, $profile, $token) {
  /*
    A FAZER:
    - Testar se o usuário existe no Banco
    - Pegar a userToken
    - Pegar o perfil do usuário

    João: Acho que é possível fazer os dois num query só.

    Se o usuário for válido insere ele no banco, e retorna true
    Se o usuário for inválido retorna false
  */


  return true;
}

$userName = $_POST['userName'];
$userEmail = $_POST['userEmail'];
$userPassword = $_POST['userPassword'];
$userPasswordRepeated = $_POST['userPasswordRepeated'];
$userProfile = $_POST['userProfile'];
$agreeTerms = $_POST['agreeTerms'];

if(
    empty($userName) || // Nome Vazio
    empty($userEmail) || // E-mail Vazio
    empty($userPassword) || // Senha Vazia
    empty($userPasswordRepeated) || // Senha repetida Vazia
    empty($userProfile) || // Perfil Vazio
    empty($agreeTerms) || // Termos vazion
    $userPassword !== $userPasswordRepeated // Senhas diferentes
  )  {
  redirect_to_register();
}

$userToken = generate_token($userEmail);

$userIsValid = test_user_validity(
  $userEmail,
  md5($userPassword),
  $userProfile,
  $userToken
);

if(!$userIsValid) {
  return redirect_to_register();
}

$msg =
  "Parabén!\n" .
  "Você foi cadastrado com sucesso.\n" .
  "Para confirmar seu cadastro e fazer uso de nossos serviços confirme\n" .
  $siteURL . "/src/actions/confirm.php?token=" . $userToken;

$msg = wordwrap($msg,150);

mail(
  $userEmail,
  "Confirmação de cadastro",
  $msg
);

// Redirect para a página inicial
header('Location: ' . '../../registrationComplete.php');
?>