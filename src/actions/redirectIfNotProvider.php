<?php
// Redireciona para a Página principal se o usuário não estiver autenticado
$userProfile = $_COOKIE['userProfile'];

if($userProfile !== 'Prestador') {
  header('Location: ' . './index.php');
};
?>