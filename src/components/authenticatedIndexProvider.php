<section class="hero is-primary is-fullheight has-text-centered">
  <?php include("./src/components/navbar.php") ?>

  <div class="hero-body">
    <div class="container is-fluid">
      <h1 class="title">
        Bem-Vindo de volta ao Serviço Fácil
      </h1>
      <h2 class="subtitle">
        Cadastre seus serviços para que outros usuário te conheçam.<br />

        <hr />
        <a class="button is-white is-outlined is-large" href="services.php">Veja nossa página de serviços!</a>
      </h2>
    </div>
  </div>
</section>