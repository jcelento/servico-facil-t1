<?php
/*
  Valida Se o usuário está autenticado pegando sua token e perfil do cookie de sessão
*/
$isAuthenticated = !empty($_COOKIE['isAuthenticated']);

if($isAuthenticated) {
  $userProfile = $_COOKIE['userProfile'];
  $userToken = $_COOKIE['userToken'];
};
?>

<nav class="nav has-shadow">
  <div class="container">
    <div class="nav-left">
      <strong class="nav-item">
        Serviço Fácil
      </strong>
    </div>

    <div class="tabs nav-right">
      <ul>
        <li><a class="nav-item is-tab" href="index.php">Início</a></li>
        <?php if($isAuthenticated) : ?>
          <li><a class="nav-item is-tab"> <?php echo $userProfile; ?>  </a></li>

          <?php if($userProfile === 'Usuário') : ?>
            <li><a class="nav-item is-tab" href=<?php echo "./services.php" ?>> Serviços </a></li>
          <?php endif; ?>

          <li><a class="nav-item is-tab" href=<?php echo "./src/actions/logout.php" ?>> Sair </a></li>
        <?php else : ?>
          <li><a class="nav-item is-tab" href=<?php echo "./register.php" ?>> Registre-se  </a></li>
          <li><a class="nav-item is-tab" href=<?php echo "./login.php" ?>> Entrar </a></li>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</nav>