<?php
include('./src/actions/redirectIfNotAuthenticated.php');
include('./src/actions/redirectIfNotUser.php');

$pageTitle = 'Página de Serviços';

$services = array(
  array(
    'name'=> 'Eletrônica',
    'items'=> array(
      'Reparos Elétricos'
    )
  ),
  array(
    'name'=> 'Mecânicos',
    'items'=> array(
      'Conserto de automóvel',
      'Troca de pneus'
    )
  ),
  array(
    'name'=> 'Eletrônica',
    'items'=> array()
  )
)
?>

<!DOCTYPE html>
<html lang="en">
  <?php include("./src/components/header.php") ?>

  <body>
    <section class="hero is-primary is-fullheight has-text-centered">
      <?php include("./src/components/navbar.php") ?>

      <div class="hero-body">
        <div class="container is-fluid">
          <h1 class="title">
            Serviços
          </h1>

          <div class="services">
            <form class="field is-grouped">
              <p class="control">
                <label class="label">Perfil:</label>
                <span class="select is-fullwidth">
                  <select name="choosedService" required>
                    <option disabled selected>Selecione Um serviço</option>

                    <?php foreach($services as $value): ?>
                    <option value=<?php echo $value['name'] ?>> <?php echo $value['name'] ?> </option>
                    <?php endforeach; ?>
                  </select>
                </span>
              </p>

              <p class="control">
                <button class="button is-primary is-outlined is-large is-fullwidth" button="submit">Pesquisar</button>
              </p>
            </form>
          </div>
        </div>
      </div>
    </section>
  </body>
</html>