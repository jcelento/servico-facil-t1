# Serviço Fácil

## Arquivos:
```
/
  # Arquivos de páginas
  /src
    /actions
      # Arquivos que não renderizem HTML, eles devem ser específicos para tarefas
    /components
      # Arquivos de HTML que se repetem em várias páginas, seu conteúdo pode conter lógicas especiais
  /assets
    /css
      # Arquivos de Estilos
    /js
      # Arquivos de Javascript
```

Por:

> João Marcus Fernandes - 1510478300064

> Jamile Celento
